# -*- coding: utf-8 -*-

{
	'name' : 'biblioteca', #nombre
	'description' : 'Biblioteca para practica de Odoo Cookbook', #descripcion
	'autor' : 'Erick Gomez',
	'category' : 'Desconocida',
	'version' : '0.1',
	'depends' : ['base','decimal_precision'],
	'data' : ['views/biblioteca_view.xml'],
	'application' : True,
	'installable' : True
}