# -*- coding: utf-8 -*-

#import logger
import logging
#Get the logger
_logger = logging.getLogger(__name__)

import os
from datetime import timedelta
from odoo import models, api, fields #importar las librerias
from odoo.addons import decimal_precision as dp #importa de addons de odoo el modulo decimal_precicion como dp
from odoo.exceptions import ValidationError, UserError #importa las llamas de error

#los modelos abstractos siempre van primeros, estos no guardan datos en la base de datos
class BaseArchivo(models.AbstractModel): 
	_name = 'base.archivo'

	active = fields.Boolean(default=True)

	def do_archivo(self):
		for record in self:
			record.active = not record.active


class BibliotecaAutor(models.Model):
	_name = 'biblioteca.autor'
	_rec_name = 'nombre'

	libro_ids = fields.Many2many('biblioteca.libro')

	nombre = fields.Char('Nombre')
	descripcion = fields.Text('Descripcion')
	web = fields.Char('Sitio Web')


class BibliotecaLibro(models.Model):
	_name = 'biblioteca.libro' #nombre del modelo
	#_inherit = 'base.archivo' #para heredar de un modelo abstracto
	_rec_name = 'nombre_corto' #muestra un mejor nombre de los registro para los campos relacionales
	_description = 'Libro de la biblioteca' #descripcion del modelo
	_order = 'fecha_lanzamiento desc, nombre' #establece la forma de ordenar los registros; [desc] para un orden inverso

	nombre = fields.Char('Titulo')
	nombre_corto = fields.Char('Nombre corto')

	categoria_ids = fields.Many2many('biblioteca.categoria', string="Categorias")

	autor_ids = fields.Many2many('res.partner', string="Autor", domain=[('editor','=','autor')])

	editor_id = fields.Many2one('res.partner', string="Editor")
	miembro_id = fields.Many2one('biblioteca.miembro', string="Miembro")
	company_id = fields.Many2one('res.company', string="Compañia")


	fecha_lanzamiento = fields.Date('Fecha de lanzamiento')
	fecha_actualizacion = fields.Date('Fecha de actualizacion')
	caratula = fields.Binary('Caratula del libro')
	notas = fields.Text('Notas internas')
	state = fields.Selection(
		[('draft','No disponible'),
		('available','Disponible'),
		('borrowed', 'Prestado'),
		('lost','Perdido')], "Estado del libro")
	descripcion = fields.Html('Descripcion')
	paginas = fields.Integer('Numero de paginas')
	raiting = fields.Float('Raiting de lectores', (14,4)) #(total, decimales)
	

	precio = fields.Float('Costo del libro', dp.get_precision('precio_libro')) #se llama a la funcion de dp por el valor establecido en el modulo
	currency_id = fields.Many2one('res.currency', string="Moneda")
	precio_menor = fields.Monetary('Precio al por menor')

	#libros = self.env['biblioteca.libro']
	#libros.browse(returned_books_ids).change_state('available')
	
	"""
	dias_pasados = fields.Float('Dias desde el lanzamiento', 
		compute="_compute_dias", 
		inverse="_inverse_dias", 
		search="_search_dias",
		store = True,
		compute_sudo = False)
	"""
	agotado = fields.Boolean('Agotado')
	#active = fields.Boolean('Activo', default=True)

	#---------------------METODOS---------------------

	def name_get(self):
		result = []
		for record in self:
			result.append((record.id, u"%s (%s)" % (record.nombre, record.fecha_lanzamiento)))
		return result
	"""
	#metodo para calcular los dias transcurridos desde que se lanzo el libro
	@api.depends('fecha_lanzamiento')
	def _compute_dias(self):
		hoy = Date.from_string(Date.today())
		for book in self.filtered('fecha_lanzamiento'):
			delta = (Date.from_string(book.fecha_lanzamiento - hoy))
			book.dias_pasados = delta.days"""
	
	#metodo para cambiar el estado del libro
	"""@api.model
	def is_allowed_transition(self, estadoViej, estadoNuev):
		allowed = [('draft','Disponible'),
			('available','Prestado'),
			('borrowed', 'Disponible'),
			('available','Perdido'),
			('borrowed','Perdido'),
			('lost','Disponible')]
		return (estadoViej, estadoNuev) in allowed

	#metodo para cambiar estado del libro
	@api.multi
	def change_state(self, estadoNuev):
		for libro in self:
			if libro.is_allowed_transition(libro.state, estadoNuev):
				libro.state = estadoNuev
			else:
				continue"""
	

	#metodo para buscar los registros vacios de otro modelo
	@api.model
	def get_all_biblioteca_miembro(self):
		biblioteca_miembro_model = self.env['biblioteca.miembro']
		return biblioteca_miembro_model.search([])

	#validacion sql para que no se repitan registros
	_sql = [('nombre_uniq', 'UNIQUE(nombre)', 'Libro ya registrado')]

	#validacion para la fecha de salida
	@api.constrains('fecha_lanzamiento')
	def _check_fecha_lanzamiento(self):
		if self.fecha_lanzamiento > fields.Date.today():
			#raise UserError("Error en la fecha %s" % self.fecha_lanzamiento)
			raise ValidationError('Error en la fecha de salida registrada')


class ResPartner(models.Model):
	_inherit = 'res.partner'
	_order = 'name'


	autor_ids = fields.One2many('biblioteca.libro', 'editor_id', string="Libros Editados")
	numero_libros2 = fields.Integer(string='Cantidad de libros editados', compute='_compute_libros_editados')

	libros_ids = fields.One2many('biblioteca.libro', 'autor_ids', string="Libros Escritos")

	escritores_ids = fields.Many2many('biblioteca.autor', string="Escritores")
	numero_escritores = fields.Integer(string='Cantidad de libros escritos', compute='_compute_escritores')

	editor = fields.Selection([('editor','Editor'),('autor','Autor')], string="Editor")


	"""
	hoy_str = fields.Date.to_string(fields.Date.today())
	val1 = {
		'name' : u'Erick',
		'email' : u'erickgomez25444@hotmail.com',
		'date' : hoy_str
	}
	val2 = {
		'name' : u'Carlos',
		'email' : u'carlosgarcia@hotmail.com',
		'date' : hoy_str
	}
	partner_val = {
		'name': u'Flying Circus',
		'email': u'm.python@example.com',
		'date': today_str,
		'is_company' : True,
		'child_ids' : [(0, 0, val1),
			(0, 0, val2)]
	}

	record = self.env['res.partner'].create(partner_val)
	"""
	#metodo para obtener numero de libros escritos
	@api.depends('escritores_ids')
	def _compute_escritores(self):
		for r in self:
			r.numero_escritores = len(r.escritores_ids)

	@api.depends('autor_ids')
	def _compute_libros_editados(self):
		for r in self:
			r.numero_libros2 = len(r.autor_ids)


	"""@api.model
	def agreagarContactos(self,partner,contactos):
		partner.ensure_one()
		if contactos:
			partner.date = fields.Date.context_today()
			partner.child_ids |= contactos

	@api.model
	def find_partners_y_contactos(self, name):
		partners = self.env['res.partner']
		domain ['|','&',('editor','like','editor'),('name','like','name'),'&',('editor','ilike','editor'),('parent_id.name','like','name')]
		return partners.search(domain)

	@api.multi
	def toggle_active(self):
		domain = ['|', ('active','=',False), ('active','=',True)]
		dones = self.search(domain)
		dones.write({'active' : '!active'})
		return True"""


class MiembrosBiblioteca(models.Model):
	_name = 'biblioteca.miembro'
	_inherits = {'res.partner' : 'partner_id'}

	partner_id = fields.Many2one('res.partner', ondelete="cascade")

	fecha_inicio = fields.Date('Miembro desde')
	fecha_fin = fields.Date('Fecha de terminacion')
	numero_miembro = fields.Char()
	libro_id = fields.Many2one('biblioteca.libro', string="Libro")

class PrestamoLibro(models.Model):
	_name='biblioteca.libro.prestamo'

	libro_id = fields.Many2one('biblioteca.libro', string="Libro")
	miembro_id = fields.Many2one('biblioteca.miembro', string="Miembro")
	state = fields.Selection([('ongoin','En Curso'),('done','Finalizado')], string="Estado", default="ongoin", required=True)


#wizard
class PrestamoLibroWizard(models.TransientModel):
	_name = 'biblioteca.prestamo.wizard'

	libro_ids = fields.Many2many('biblioteca.libro', string="Libro")
	miembro_id = fields.Many2one('biblioteca.miembro', string="Miembro")

	fecha_esperada = fields.Date('Esperando hasta')

	#metodos para establecer a quien se presto el libro
	@api.multi
	def registro_prestamos(self):
		for wizard in self:
			miembro = wizard.miembro_id
			libros = wizard.libro_ids
			prestamo = self.env['biblioteca.libro.prestamo']
			for libro in wizard.libro_ids:
				valor = self._preparar_Prestamo(libro)
				prestamo.create(valor)

	@api.multi
	def _preparar_Prestamo(self,libro):
		return {'miembro_id' : self.miembro_id.id,
			'libro_id' : libro.id}

class PrestamoLibroWizard(models.TransientModel):
	_inherit = 'biblioteca.prestamo.wizard'

	#metodo heredado para añadir el tiempo de prestamo del libro
	def _preparar_Prestamo(self,libro):
		#_logger.info('\n\n %s \n' % (dir(fields)))
		valor = super(PrestamoLibroWizard, self)._preparar_Prestamo(libro)
		duracion_prestamo = self.miembro_id.duracion_prestamo

		hoy = fields.Date.today()
		_logger.info('\n\n %s \n' % (hoy))
		hoy_str = fields.Date.from_string(hoy)
		esperado = hoy_str + timedelta(days = duracion_prestamo)
		valor.update(
				{'fecha_esperada' : fields.Date.to_string(esperado)}
			)
		return valor



class PrestamoLibro(models.Model):
	_inherit = 'biblioteca.libro.prestamo'

	fecha_esperada = fields.Date('Fecha de regreso:')

class MiembrosBiblioteca(models.Model):
	_inherit = 'biblioteca.miembro'

	duracion_prestamo = fields.Integer("Tiempo de prestamo (dias)", default=15)
	dia_prestamo = fields.Date()

	#dia_final = fields.Date("Dia de entrega", readonly=True, related=dia_prestamo)

	@api.onchange('libro_id')
	def _onchange_libro_id(self):
		dia_prestamo = self.libro_id.fecha_esperada
"""class MismoModelo(models.Model):
	datos = fields.Text('Datos')

	@api.multi
	def save(self, nombrearchivo):
		if '/' in nombrearchivo or '\\' in nombrearchivo:
			raise UserError('Nombre %s ilegal' % nombrearchivo)
		try:
			path = os.path.join('/opt/export', nombrearchivo)
			with open(path, 'w') as fobj:
				for record in self:
					fobj.write(record.datos)
					fobj.write('\n')
		except (IOError, OSError) as exc:
			mensaje = 'No se puede guardar el archivo %s' % exc
			raise UserError(mensaje)
"""

class ResCompany(models.Model):
	_inherit = "res.company"
	
	partners = fields.Many2many('res.partner', string="Partners", domain=[('editor','=','editor')])
	autores = fields.Many2many('res.partner', string="Autores", domain=[('editor','=','autor')])
	libros_ids = fields.Many2many('biblioteca.libro', 'company_id', domain=[('autor_ids','=','partners')])
	miembros = fields.Many2many('biblioteca.miembro')