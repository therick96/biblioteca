# -*- coding: utf-8 -*-

from odoo import models, api, fields #importar las librerias

class CategoriaLibro(models.Model):
	_name = 'biblioteca.categoria'
	_rec_name = 'nombre'

	nombre = fields.Char('Categoria')
	parent_id = fields.Many2one('biblioteca.categoria', ondelete="restrict", index=True)
	child_ids = fields.One2many('biblioteca.categoria', 'parent_id', string="Sub-Categorias")

	parent_store = True #soporte especial para jerarquias
	parent_left = fields.Integer(index=True) #util para lecturas
	parent_right = fields.Integer(index=True) #util para lecturas

	libros_ids = fields.Many2many('biblioteca.libro')

	#metodo para evitar recursividad en las busquedas
	@api.constrains('parent_id')
	def _check_hierarchy(self):
		if not self._check_recursion():
			raise models.ValidationError('Error, no puedes crear categorias recursivas')
